package com.chs.feature.qb.configuration.repository;

import com.chs.feature.qb.configuration.domain.QbAccount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface QbAccountRepository extends JpaRepository<QbAccount, Long> {
    Optional<QbAccount> findByHookId(Long hookId);
}
