package com.chs.feature.qb.coa.data;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class EntityRefData {
    private String value;
}
