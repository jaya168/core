package com.chs.utils;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

public class HttpService {

    private static final HttpClient CLIENT = HttpClient.newBuilder()
            .connectTimeout(Duration.ofSeconds(10))
            .followRedirects(HttpClient.Redirect.NEVER)
            .build();

    public static String TOKEN = "";

    public static HttpResponse<String> get(final String uri) {
        try {
            return CLIENT.send(request(uri).GET().build(), HttpResponse.BodyHandlers.ofString());
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static HttpResponse<String> post(final String uri,
                                            final String body) {
        try {
            return CLIENT.send(
                    request(uri).POST(HttpRequest.BodyPublishers.ofString(body)).build(),
                    HttpResponse.BodyHandlers.ofString());
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static HttpResponse<String> put(final String uri,
                                           final String body) {
        try {
            return CLIENT.send(
                    request(uri).PUT(HttpRequest.BodyPublishers.ofString(body)).build(),
                    HttpResponse.BodyHandlers.ofString());
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static HttpResponse<String> delete(final String uri) {
        try {
            return CLIENT.send(request(uri).DELETE().build(), HttpResponse.BodyHandlers.ofString());
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static HttpRequest.Builder request(final String uri) {
        return HttpRequest.newBuilder()
                .uri(URI.create(uri))
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + TOKEN)
                .timeout(Duration.ofSeconds(10));
    }
}
