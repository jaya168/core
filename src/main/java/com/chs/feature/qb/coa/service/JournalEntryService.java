package com.chs.feature.qb.coa.service;

import com.chs.feature.accounting.domain.JournalEntryPostingType;
import com.chs.feature.qb.coa.data.*;
import com.chs.feature.qb.coa.domain.COAMapping;
import com.chs.feature.qb.coa.domain.QbGeneralLedger;
import com.chs.feature.qb.coa.repository.COAMappingRepository;
import com.chs.feature.qb.configuration.domain.QbAccount;
import com.chs.feature.qb.configuration.repository.QbAccountRepository;
import com.chs.feature.qb.configuration.service.QbHttpService;
import com.chs.feature.webhook.domain.Bongloy;
import com.chs.persistence.exception.ResourceNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.chs.feature.accounting.domain.JournalEntryPostingType.CREDIT;
import static com.chs.feature.accounting.domain.JournalEntryPostingType.DEBIT;
import static com.chs.feature.qb.coa.data.DetailTypeConstants.JOURNAL_ENTRY_LINE_DETAIL;

@Slf4j
@Service
@RequiredArgsConstructor
public class JournalEntryService {

    private static final BigDecimal FEE_RATE = BigDecimal.valueOf(0.013);
    private static final BigDecimal INCOME_RATE = BigDecimal.valueOf(0.987);
    private static final BigDecimal TAX_RATE = BigDecimal.valueOf(0.1);
    private static final BigDecimal HUNDRED = BigDecimal.valueOf(100);

    private final QbAccountRepository repository;
    private final QbHttpService qbHttpService;
    private final COAMappingRepository mappingRepository;
    private final ObjectMapper objectMapper;

    @Async
    public void create(Bongloy bongloy, Long hookId) {
        log.info("creating entry in qb");
        var totalAmount = bongloy.getAmount().divide(HUNDRED, 8, RoundingMode.UNNECESSARY);

        // get account configuration
        var qb = repository.findByHookId(hookId)
                .orElseThrow(() -> new ResourceNotFoundException(QbAccount.class, "" + hookId));

        var coa = mappingRepository.findByQbAccount(qb)
                .orElseThrow(() -> new ResourceNotFoundException(COAMapping.class, qb.getId()));
        var json = Map.of("Line", buildJournalEntryData(coa, totalAmount));
        try {
            qbHttpService.createEntry(objectMapper.writeValueAsString(json), qb);
        } catch (JsonProcessingException e) {
            log.error("Error to build json", e);
        } catch (Exception e) {
            log.error("Error to create journal entry", e);
        }
    }

    private List<JournalEntryData> buildJournalEntryData(COAMapping coaMapping, BigDecimal totalAmount) {
        if (coaMapping.getGeneralLedgers() == null) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Chart of account mapping is not completed yet");
        }

        final var taxAmount = coaMapping.isHasTax() ? totalAmount.multiply(TAX_RATE) : BigDecimal.ZERO;
        final var grossAmount = totalAmount.subtract(taxAmount);
        final var vendorId = coaMapping.getVendor().getVendorId();

        return coaMapping.getGeneralLedgers()
                .parallelStream()
                .map(gl -> buildJournalEntryData(gl, vendorId, grossAmount, taxAmount))
                .collect(Collectors.toList());
    }

    private JournalEntryData buildJournalEntryData(QbGeneralLedger gl,
                                                   String vendorId,
                                                   BigDecimal grossAmount,
                                                   BigDecimal taxAmount) {
        String desc;
        BigDecimal amount;
        JournalEntryPostingType postingType;

        switch (gl.getClassification()) {
            case INCOME:
                amount = grossAmount.multiply(INCOME_RATE);
                desc = "Amount from Bongloy payment";
                postingType = DEBIT;
                break;
            case EXPENSE:
                amount = grossAmount.multiply(FEE_RATE);
                desc = "Fee charge for Bongloy payment portal";
                postingType = DEBIT;
                break;
            case RECEIVABLE:
                desc = "Receivable amount";
                amount = grossAmount;
                postingType = CREDIT;
                break;
            case TAX:
                desc = "VAT tax 10%";
                amount = taxAmount;
                postingType = CREDIT;
                break;
            default:
                throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "Invalid configuration, please recheck configuration");
        }
        return buildJournalEntryData(
                amount,
                postingType,
                desc,
                gl.getAccountId(),
                vendorId);
    }

    private JournalEntryData buildJournalEntryData(BigDecimal amount,
                                                   JournalEntryPostingType postingType,
                                                   String description,
                                                   String glId,
                                                   String vendorId) {
        return new JournalEntryData()
                .setAmount(amount)
                .setDescription(description)
                .setDetailType(JOURNAL_ENTRY_LINE_DETAIL)
                .setJournalEntryLineDetail(new JournalEntryLineDetailData()
                        .setPostingType(postingType.getName())
                        .setAccountRef(new AccountRefData().setValue(glId))
                        .setEntity(new EntityData()
                                .setType("Customer")
                                .setEntityRef(new EntityRefData().setValue(vendorId))));
    }
}
