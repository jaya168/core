package com.chs.feature.qb.configuration.controller;

import com.chs.feature.qb.configuration.data.PaymentGateWay;
import com.chs.feature.qb.configuration.domain.QbAccount;
import com.chs.feature.qb.configuration.repository.QbAccountRepository;
import com.chs.feature.qb.configuration.service.RefreshTokenService;
import com.chs.persistence.exception.ResourceNotFoundException;
import com.chs.persistence.filter.FilterConfig;
import com.chs.persistence.service.EntityDataMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("qb")
@RequiredArgsConstructor
public class QBController {

    private final QbAccountRepository repository;
    private final EntityDataMapper entityDataMapper;
    private final RefreshTokenService tokenService;
    private final FilterConfig filterConfig;

    @Value("${server.ip}")
    private String serverAddress;

    @Value("${server.port}")
    private String serverPort;

    @PostMapping
    public QbAccount create(@RequestBody @Valid QbAccount account) {
        return repository.save(account);
    }

    @GetMapping
    public List<QbAccount> getAll() {
        filterConfig.enableFilter();
        return repository.findAll();
    }

    @GetMapping("{id}")
    public QbAccount getOne(@PathVariable Long id) {
        filterConfig.enableFilter();
        return repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(QbAccount.class, id));
    }

    @PutMapping("{id}")
    @Transactional
    public QbAccount update(@PathVariable Long id, @RequestBody QbAccount account) {
        filterConfig.enableFilter();
        var qb = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(QbAccount.class, id));
        var newQb = entityDataMapper.mapObject(account, qb, QbAccount.class);
        return repository.save(newQb);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
        filterConfig.enableFilter();
        repository.deleteById(id);
    }

    @GetMapping("{id}/hookurl")
    public Map<String, String> getHookUrl(@PathVariable Long id, @RequestParam(defaultValue = "BONGLOY") PaymentGateWay gateWay) {
        filterConfig.enableFilter();
        var qb = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(QbAccount.class, id));
        switch (gateWay) {
            case BONGLOY:
                return Map.of("url", String.format("http://%s:%s/webhook/bongloy/%s", serverAddress, serverPort, qb.getHookId()));
            case WING:
                return Map.of("url", String.format("http://%s:%s/webhook/wing/%s", serverAddress, serverPort, qb.getHookId()));
            default:
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Invalid payment gateway");
        }
    }

    @PostMapping("{id}/validate/connection")
    public Map<String, Object> validateConnection(@PathVariable Long id) {
        filterConfig.enableFilter();
        var qb = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(QbAccount.class, id));
        try {
            tokenService.refreshToken(qb);
            qb.setConfigurationSuccess(true);
            repository.save(qb);
            return Map.of("success", true, "message", "connected successfully");
        } catch (RuntimeException e) {
            log.error("fail to connect to QB");
            return Map.of("success", false, "message", e.getMessage());
        }
    }
}
