package com.chs.feature.qb.coa.domain;

import com.chs.feature.qb.configuration.domain.QbAccount;
import com.chs.persistence.domain.AuditingEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class COAMapping extends AuditingEntity {

    @OneToOne
    private QbAccount qbAccount;

    @Embedded
    private QbVendor vendor;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name = "coa_mapping_id")
    private Set<QbGeneralLedger> generalLedgers;

    @Transient
    private boolean hasTax;

    public boolean isHasTax() {
        return getGeneralLedgers() != null && getGeneralLedgers().size() == 4;
    }
}
