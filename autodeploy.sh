echo > nohup.out
git pull
kill $(ps -aux | grep bookkeeping |grep -v grep | awk '{print $2}')
./gradlew bootJar
kill $(ps -aux | grep gradle |grep -v grep | awk '{print $2}')
sleep 5
nohup java -Xms2048m -Xmx2048m -XX:MetaspaceSize=128M -XX:MaxMetaspaceSize=128M -XX:+UseParallelGC -jar ./build/libs/bookkeeping-0.0.1.jar &
