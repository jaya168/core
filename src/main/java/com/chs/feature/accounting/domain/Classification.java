package com.chs.feature.accounting.domain;

import java.util.EnumSet;
import java.util.Set;

public enum Classification {
    INCOME,
    EXPENSE,
    RECEIVABLE,
    TAX;

    public static final Set<Classification> REQUIRED_CLASSIFICATIONS = Set.of(
            INCOME,
            EXPENSE,
            RECEIVABLE);

    public static final Set<Classification> ALL_CLASSIFICATIONS = EnumSet.allOf(Classification.class);
}
