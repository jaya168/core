package com.chs.feature.webhook.controller;

import com.chs.feature.qb.coa.service.JournalEntryService;
import com.chs.feature.webhook.data.BongloyData;
import com.chs.feature.webhook.data.mapper.BongloyMapper;
import com.chs.feature.webhook.repository.BongloyRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("webhook/bongloy")
public class BongloyController {

    private static final String PAYMENT_INTENT_TYPE = "payment_intent.succeeded";

    private final ObjectMapper objectMapper;
    private final BongloyMapper mapper;
    private final BongloyRepository repository;
    private final JournalEntryService journalEntryService;

    @PostMapping("{hookId}")
    @Transactional
    public Map<String, Object> receiveRequest(@PathVariable final Long hookId,
                                              @RequestBody final String body) {
        log.info("receiving a webhook");
        if (!StringUtils.hasText(body)) {
            return Map.of("success", false, "message", "body is empty");
        }

        try {
            final var jsonNode = objectMapper.readValue(body, JsonNode.class);
            if (jsonNode.has("type") && jsonNode.get("type").asText().equalsIgnoreCase(PAYMENT_INTENT_TYPE)) {
                Optional.of(jsonNode)
                        .map(node -> node.get("data"))
                        .map(node -> node.get("object"))
                        .map(node -> objectMapper.convertValue(node, BongloyData.class))
                        .map(mapper::map)
                        .ifPresent(bongloy -> {
                            repository.save(bongloy);
                            journalEntryService.create(bongloy, hookId);
                        });
            }
            return Map.of("success", true);
        } catch (JsonProcessingException e) {
            log.error("fail to deserialize json");
            return Map.of("success", false, "message", "json invalid");
        }
    }
}
