package com.chs.persistence.filter;

import com.chs.appconfiguration.utils.ApplicationSecurityContext;
import com.chs.organization.user.domain.UserRole;
import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;

@Component
@RequiredArgsConstructor
public class FilterConfig {

    private final ApplicationSecurityContext context;
    private final EntityManager entityManager;

    public void enableFilter() {
        if (context.authenticatedUser() == null) {
            var session = entityManager.unwrap(Session.class);
            session.enableFilter("userFilter").setParameter("userId", 0L);
        } else if (context.authenticatedUser() != null
                && UserRole.USER.equals(context.authenticatedUser().getRole())) {
            var session = entityManager.unwrap(Session.class);
            session.enableFilter("userFilter").setParameter("userId", context.authenticatedUser().getId());
        }
    }

    /*
    @Bean
    @Primary
    public PlatformTransactionManager transactionManager(ObjectProvider<TransactionManagerCustomizers> transactionManagerCustomizers) {
        var transactionManager = new JpaTransactionManager() {
            @Override
            protected EntityManager createEntityManagerForTransaction() {
                final var entityManager = super.createEntityManagerForTransaction();
                System.out.println("here hre");
                if (context.authenticatedUser() != null && UserRole.USER.equals(context.authenticatedUser().getRole())) {
                    System.out.println("is executed");
                    var session = entityManager.unwrap(Session.class);
                    session.enableFilter("userFilter").setParameter("userId", context.authenticatedUser().getId());
                }
                return entityManager;
            }
        };
        transactionManagerCustomizers.ifAvailable((customizers) -> customizers.customize(transactionManager));
        return transactionManager;
    }
     */
}
