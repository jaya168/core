package com.chs.feature.webhook.data;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BongloyData {
    String id;
    BigDecimal amount;
    String object;
    String status;
    Map<String, Object> charges;
    Long created;
    String currency;
    @JsonAlias("client_secret")
    String clientSecret;
    @JsonAlias("payment_method_types")
    List<String> paymentMethodTypes;
}
