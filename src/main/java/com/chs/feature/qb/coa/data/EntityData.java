package com.chs.feature.qb.coa.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class EntityData {

    @JsonProperty("Type")
    private String type;

    @JsonProperty("EntityRef")
    private EntityRefData entityRef;
}
