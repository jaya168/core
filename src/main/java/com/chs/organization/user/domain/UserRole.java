package com.chs.organization.user.domain;

public enum UserRole {
    ADMIN,
    USER
}
