package com.chs.feature.qb.configuration.service;

import com.chs.feature.qb.configuration.domain.QbAccount;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;

@Slf4j
@Service
public class QbHttpService {

    private static final HttpClient CLIENT = HttpClient.newBuilder()
            .connectTimeout(Duration.ofSeconds(10))
            .followRedirects(HttpClient.Redirect.NEVER)
            .build();

    private HttpRequest.Builder request(String uri, String token) {
        return HttpRequest.newBuilder()
                .uri(URI.create(uri))
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer " + token)
                .timeout(Duration.ofSeconds(10));
    }

    public void createEntry(String body, QbAccount account) {
        var url = String.format("%s/%s/journalentry?minorversion=%s", account.getUrl(), account.getCompanyId(), account.getMinorVersion());
        post(url, body, account.getToken());
    }

    private void post(final String uri,
                      final String body,
                      final String token) {
        try {
            var request = request(uri, token)
                    .POST(HttpRequest.BodyPublishers.ofString(body))
                    .build();
            var response = CLIENT.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == HttpStatus.OK.value()) {
                log.info("create entry successfully");
            }
        } catch (final IOException | InterruptedException e) {
            log.error("fail to create entry", e);
            throw new RuntimeException(e);
        }
    }
}
