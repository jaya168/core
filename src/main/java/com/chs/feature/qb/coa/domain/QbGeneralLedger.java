package com.chs.feature.qb.coa.domain;

import com.chs.feature.accounting.domain.Classification;
import com.chs.persistence.domain.VersionEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@Accessors(chain = true)
@Entity
public class QbGeneralLedger extends VersionEntity {

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "coa_mapping_id")
    private COAMapping coaMapping;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Classification classification;

    @NotBlank
    private String accountName;

    @NotBlank
    private String accountId;

    private String acctNum;
}
