package com.chs.feature.qb.coa.repository;

import com.chs.feature.qb.coa.domain.COAMapping;
import com.chs.feature.qb.configuration.domain.QbAccount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface COAMappingRepository extends JpaRepository<COAMapping, Long> {

    void deleteByQbAccount(QbAccount qbAccount);

    Optional<COAMapping> findByQbAccountId(Long id);

    Optional<COAMapping> findByQbAccount(QbAccount qbAccount);
}
