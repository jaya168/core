package com.chs.feature.webhook.repository;

import com.chs.feature.webhook.domain.Bongloy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BongloyRepository extends JpaRepository<Bongloy, String> {
}
