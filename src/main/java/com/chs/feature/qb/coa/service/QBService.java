package com.chs.feature.qb.coa.service;

import com.chs.feature.qb.configuration.domain.QbAccount;
import com.intuit.ipp.core.Context;
import com.intuit.ipp.core.ServiceType;
import com.intuit.ipp.data.Account;
import com.intuit.ipp.data.Customer;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.security.OAuth2Authorizer;
import com.intuit.ipp.services.DataService;
import com.intuit.ipp.util.Config;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@Slf4j
public class QBService {

    @SuppressWarnings("unchecked")
    public List<Account> getAllAccount(QbAccount account) {
        try {
            var dataService = getDataService(account);
            var sql = "Select * from Account where Name like '%JAYA%'";
            var queryResult = dataService.executeQuery(sql);
            return (List<Account>) queryResult.getEntities();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
        }
    }

    @SuppressWarnings("unchecked")
    public List<Customer> getAllVendor(QbAccount account) {
        try {
            var dataService = getDataService(account);
            var sql = "Select * from Customer";
            var queryResult = dataService.executeQuery(sql);
            return (List<Customer>) queryResult.getEntities();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
        }
    }

    public Account getById(QbAccount account, String id) {
        try {
            var dataService = getDataService(account);
            var sql = String.format("Select * from Account where id = '%s'", id);
            var queryResult = dataService.executeQuery(sql);
            return (Account) queryResult.getEntities().get(0);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
        }
    }

    public DataService getDataService(QbAccount account) {
        try {
            Config.setProperty(Config.BASE_URL_QBO, account.getUrl());
            var oauth = new OAuth2Authorizer(account.getToken());
            var context = new Context(oauth, ServiceType.QBO, account.getCompanyId());
            return new DataService(context);
        } catch (FMSException e) {
            log.error("fail to get data service");
            throw new RuntimeException(e);
        }
    }
}
