package com.chs.feature.qb.coa.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class JournalEntryLineDetailData {

    @JsonProperty("PostingType")
    private String postingType;

    @JsonProperty("Entity")
    private EntityData entity;

    @JsonProperty("AccountRef")
    private AccountRefData accountRef;
}
