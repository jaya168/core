package com.chs.organization.info.repository;

import com.chs.organization.info.domain.OrganizationDetail;
import com.chs.organization.user.domain.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrganizationDetailRepository extends JpaRepository<OrganizationDetail, Long> {
    List<OrganizationDetail> findByCreatedBy(AppUser appUser);
}
