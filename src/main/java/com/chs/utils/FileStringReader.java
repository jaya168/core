package com.chs.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Slf4j
public class FileStringReader {

    public static FileStringReader instance() {
        return new FileStringReader();
    }

    public String read(final String path) {
        final var result = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(path)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line).append("\n");
            }
            return result.toString();
        } catch (final IOException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public String read(final String path, final Object... values) {
        return String.format(read(path), values);
    }
}
