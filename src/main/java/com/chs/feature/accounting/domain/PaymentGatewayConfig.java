package com.chs.feature.accounting.domain;

import com.chs.feature.qb.configuration.domain.QbAccount;
import com.chs.persistence.domain.AuditingEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(uniqueConstraints = @UniqueConstraint(name = "unique_gw", columnNames = {"gateway", "account_id"}))
@Accessors(chain = true)
public class PaymentGatewayConfig extends AuditingEntity {

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "account_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private QbAccount account;

    @Enumerated(EnumType.STRING)
    private Gateway gateway;
}
