package com.chs.organization.user.data;

import com.chs.organization.user.domain.AppUser;
import com.chs.organization.user.domain.UserRole;
import com.chs.persistence.mapper.DataMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserDataMapper extends DataMapper<AppUser, UserData> {

    final PasswordEncoder encoder;

    @Override
    public AppUser map(UserData data) {
        final var user = super.map(data);
        if (data.getPassword() != null) {
            user.setPassword(encoder.encode(data.getPassword()));
        }
        user.setRole(UserRole.USER);
        return user;
    }
}
