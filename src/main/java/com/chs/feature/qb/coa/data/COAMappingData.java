package com.chs.feature.qb.coa.data;

import com.chs.feature.qb.coa.domain.QbGeneralLedger;
import com.chs.feature.qb.coa.domain.QbVendor;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Accessors(chain = true)
public class COAMappingData {

    @NotNull
    private QbVendor vendor;

    @NotNull
    @Size(max = 4, min = 3)
    private Set<QbGeneralLedger> generalLedgers;
}
