package com.chs.organization.user.domain;

public enum UserStatus {
    ACTIVE,
    BLOCKED,
    DELETED
}
