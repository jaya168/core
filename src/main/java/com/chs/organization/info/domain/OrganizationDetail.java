package com.chs.organization.info.domain;

import com.chs.persistence.converter.StringListConverter;
import com.chs.persistence.domain.AuditingEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Getter
@Setter
@Table(uniqueConstraints = @UniqueConstraint(name = "unique_org", columnNames = "created_by"))
@Accessors(chain = true)
public class OrganizationDetail extends AuditingEntity {

    @NotNull
    @Column(nullable = false)
    private String name;

    private String image;

    private String details;

    @Convert(converter = StringListConverter.class)
    private List<String> contacts;

    private String address;
}
