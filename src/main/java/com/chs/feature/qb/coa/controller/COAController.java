package com.chs.feature.qb.coa.controller;

import com.chs.feature.qb.coa.data.COAMappingData;
import com.chs.feature.qb.coa.domain.COAMapping;
import com.chs.feature.qb.coa.domain.QbGeneralLedger;
import com.chs.feature.qb.coa.repository.COAMappingRepository;
import com.chs.feature.qb.coa.service.QBService;
import com.chs.feature.qb.configuration.domain.QbAccount;
import com.chs.feature.qb.configuration.repository.QbAccountRepository;
import com.chs.persistence.exception.ResourceNotFoundException;
import com.chs.persistence.filter.FilterConfig;
import com.intuit.ipp.data.Account;
import com.intuit.ipp.data.Customer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.chs.feature.accounting.domain.Classification.ALL_CLASSIFICATIONS;
import static com.chs.feature.accounting.domain.Classification.REQUIRED_CLASSIFICATIONS;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("qb/{id}")
public class COAController {

    private final QBService qbService;
    private final FilterConfig filterConfig;
    private final QbAccountRepository repository;
    private final COAMappingRepository mappingRepository;

    @GetMapping("account")
    public List<Account> getAccount(@PathVariable Long id) {
        filterConfig.enableFilter();
        var qb = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(QbAccount.class, id));
        return qbService.getAllAccount(qb);
    }

    @GetMapping("vendor")
    public List<Customer> getVendor(@PathVariable Long id) {
        filterConfig.enableFilter();
        var qb = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(QbAccount.class, id));
        return qbService.getAllVendor(qb);
    }

    @PostMapping("account/mapping")
    @Transactional
    public Map<String, Object> mapping(@PathVariable Long id, @Valid @RequestBody COAMappingData data) {

        final var classifications = data.getGeneralLedgers().size() == 3
                ? REQUIRED_CLASSIFICATIONS
                : ALL_CLASSIFICATIONS;
        // validate data
        var containAll = data.getGeneralLedgers()
                .stream()
                .map(QbGeneralLedger::getClassification)
                .collect(Collectors.toSet())
                .containsAll(classifications);
        if (!containAll) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "need all type classifications of accounting:" + classifications.toString());
        }

        filterConfig.enableFilter();
        var qb = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(QbAccount.class, id));

        var coaMapping = new COAMapping()
                .setQbAccount(qb)
                .setGeneralLedgers(data.getGeneralLedgers())
                .setVendor(data.getVendor());

        // clean up existing configuration
        mappingRepository.deleteByQbAccount(qb);
        // save new value
        mappingRepository.save(coaMapping);

        return Map.of("success", true);
    }

    @GetMapping("account/mapping")
    public COAMappingData getCOAMapping(@PathVariable Long id) {
        filterConfig.enableFilter();
        return mappingRepository.findByQbAccountId(id)
                .map(coaMapping ->
                        new COAMappingData()
                                .setVendor(coaMapping.getVendor())
                                .setGeneralLedgers(coaMapping.getGeneralLedgers()))
                .orElse(null);
    }
}
