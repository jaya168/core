package com.chs.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class PasswordInvalidException extends ResponseStatusException {

    public PasswordInvalidException() {
        super(HttpStatus.BAD_REQUEST, "Old password is invalid");
    }
}
