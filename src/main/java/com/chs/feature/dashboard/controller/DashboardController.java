package com.chs.feature.dashboard.controller;

import com.chs.feature.accounting.repository.PaymentGatewayConfigRepository;
import com.chs.feature.webhook.domain.Bongloy;
import com.chs.feature.webhook.repository.BongloyRepository;
import com.chs.persistence.filter.FilterConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/dashboard")
public class DashboardController {

    private final FilterConfig filterConfig;
    private final PaymentGatewayConfigRepository paymentGatewayConfigRepository;
    private final BongloyRepository bongloyRepository;

    @GetMapping
    public Map<String, Object> getPaymentPortal() {
        filterConfig.enableFilter();
        var payment = paymentGatewayConfigRepository.findAll().size();
        var bongloy = bongloyRepository.findAll();
        var tx = bongloy.size();
        var amount = bongloy.parallelStream()
                .map(Bongloy::getAmount)
                .reduce(BigDecimal::add);
        return Map.of(
                "payment", payment,
                "transaction", tx,
                "amount", amount
        );
    }
}
