package com.chs.feature.qb.configuration.service;

import com.intuit.oauth2.client.OAuth2PlatformClient;
import com.intuit.oauth2.config.Environment;
import com.intuit.oauth2.config.OAuth2Config;
import com.intuit.oauth2.data.BearerTokenResponse;
import com.intuit.oauth2.exception.OAuthException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Oauth2Utils {

    public static BearerTokenResponse getRefreshToken(String clientId,
                                                      String clientSecrete,
                                                      String refreshToken) {
        try {
            //Prepare the config
            OAuth2Config oauth2Config = new OAuth2Config
                    .OAuth2ConfigBuilder(clientId, clientSecrete)
                    .callDiscoveryAPI(Environment.SANDBOX)
                    .buildConfig();

            //Prepare OAuth2PlatformClient
            OAuth2PlatformClient client = new OAuth2PlatformClient(oauth2Config);

            //Call refresh endpoint
            return client.refreshToken(refreshToken);
        } catch (OAuthException e) {
            log.error("error to refresh token", e);
            throw new RuntimeException(e);
        }
    }
}
