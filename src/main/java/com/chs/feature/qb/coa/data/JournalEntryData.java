package com.chs.feature.qb.coa.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class JournalEntryData {

    @JsonProperty("Description")
    private String description;

    @JsonProperty("Amount")
    private BigDecimal amount;

    @JsonProperty("DetailType")
    private String detailType;

    @JsonProperty("JournalEntryLineDetail")
    private JournalEntryLineDetailData journalEntryLineDetail;
}
