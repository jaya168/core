package com.chs.feature.qb.configuration.service;

import com.chs.feature.qb.configuration.domain.QbAccount;
import com.chs.feature.qb.configuration.repository.QbAccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class RefreshTokenService {

    private final QbAccountRepository repository;

    @Scheduled(fixedDelay = 30 * 60 * 1000)
    protected void refreshToken() {
        log.info("refreshing qb token");
        repository.findAll().forEach(account -> {
            if (!account.isConfigurationSuccess()) return;
            try {
                refreshToken(account);
            } catch (Exception e) {
                log.error("fail to refresh token for account:" + account.getId());
            }
        });
    }

    @Transactional
    public void refreshToken(QbAccount account) {
        var response = Oauth2Utils.getRefreshToken(
                account.getClientId(),
                account.getClientSecret(),
                account.getRefreshToken());
        account.setRefreshToken(response.getRefreshToken());
        account.setToken(response.getAccessToken());
        repository.save(account);
    }
}
