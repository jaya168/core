INSERT INTO bookkeeping.users
(id, version, email, mobile, password, `role`, status, user_name)
VALUES(1, 0, 'admin@mail.com', NULL, '$2a$10$w2kHVB/rCZk1FVJOa0P9BOWumTwjj7qJxIAhKfqDXuI/zzBsY7jZy', 'ADMIN', 'ACTIVE', 'admin');

insert into qb_account(id,version,created_at,created_by,url,client_id,client_secret,company_id,token,refresh_token,environment,minor_version)
values(1,0,now(),1,
       'https://sandbox-quickbooks.api.intuit.com/v3/company/',
       'ABJYOXx4o0LnAShXkaML1YW7MM5aU2k5Ev24WZocf9Nx3iRjba',
       'O8JQX0mjrMlYrMOXeBCG05zb0phHh45ZcLOEofmO',
       '4620816365164366260',
       '',
       'AB11625543537sSFL0QG4TbmMpCw1bJ69Y48I8U1EnCr6cfuFT',
       'SANDBOX',
        14);