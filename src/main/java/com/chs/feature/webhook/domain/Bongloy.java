package com.chs.feature.webhook.domain;

import com.chs.persistence.converter.JsonObjectConverter;
import com.chs.persistence.converter.StringListConverter;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Bongloy {
    @Id
    String id;

    BigDecimal amount;

    String object;

    String status;

    @Convert(converter = JsonObjectConverter.class)
    @Column(columnDefinition = "text")
    Map<String, Object> charges;

    Long created;

    String currency;

    String clientSecret;

    @Convert(converter = StringListConverter.class)
    @Column(columnDefinition = "text")
    List<String> paymentMethodTypes;
}
