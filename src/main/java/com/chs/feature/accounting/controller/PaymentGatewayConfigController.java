package com.chs.feature.accounting.controller;

import com.chs.feature.accounting.domain.Gateway;
import com.chs.feature.accounting.domain.PaymentGatewayConfig;
import com.chs.feature.accounting.repository.PaymentGatewayConfigRepository;
import com.chs.feature.qb.configuration.domain.QbAccount;
import com.chs.feature.qb.configuration.repository.QbAccountRepository;
import com.chs.persistence.exception.ResourceNotFoundException;
import com.chs.persistence.filter.FilterConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("qb/{id}/payment")
public class PaymentGatewayConfigController {

    private final PaymentGatewayConfigRepository repository;
    private final QbAccountRepository accountRepository;
    private final FilterConfig filterConfig;

    @PostMapping
    public Map<String, Object> addPayment(@PathVariable Long id, @RequestParam(defaultValue = "BONGLOY") Gateway gateway) {
        filterConfig.enableFilter();
        var qb = accountRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(QbAccount.class, id));

        try {
            var config = new PaymentGatewayConfig()
                    .setAccount(qb)
                    .setGateway(gateway);
            repository.save(config);
            return Map.of("success", true);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        }
    }

    @GetMapping
    public List<PaymentGatewayConfig> getAll(@PathVariable Long id) {
        filterConfig.enableFilter();
        var qb = accountRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(QbAccount.class, id));
        return repository.findAllByAccount(qb);
    }

    @DeleteMapping("{paymentId}")
    public Map<String, Object> deletePayment(@PathVariable Long id, @PathVariable Long paymentId) {
        filterConfig.enableFilter();
        repository.deleteById(paymentId);
        return Map.of("success", true);
    }
}
