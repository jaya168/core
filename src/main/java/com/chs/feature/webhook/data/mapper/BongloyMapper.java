package com.chs.feature.webhook.data.mapper;

import com.chs.feature.webhook.data.BongloyData;
import com.chs.feature.webhook.domain.Bongloy;
import com.chs.persistence.mapper.DataMapper;
import org.springframework.stereotype.Component;

@Component
public class BongloyMapper extends DataMapper<Bongloy, BongloyData> {
}
