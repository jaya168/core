package com.chs.feature.qb.configuration.domain;

import com.chs.persistence.domain.AuditingEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.intuit.oauth2.config.Environment;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Table(name = "qb_account")
@JsonIgnoreProperties(value = "configurationSuccess", allowGetters = true)
public class QbAccount extends AuditingEntity {

    @Column(nullable = false)
    private String url;

    @NotNull
    @Column(unique = true, nullable = false)
    private String clientId;

    @NotNull
    @Column(nullable = false)
    private String clientSecret;

    @NotNull
    @Column(unique = true, nullable = false)
    private String companyId;

    @JsonIgnore
    @Column(columnDefinition = "text")
    private String token;

    private String refreshToken;

    @Enumerated(EnumType.STRING)
    private Environment environment;

    private int minorVersion;

    @Column(unique = true, updatable = false)
    private Long hookId;

    private boolean configurationSuccess;

    @PrePersist
    @PreUpdate
    private void prePersist() {
        if (environment == null) {
            environment = Environment.SANDBOX;
        }
        if (hookId == null) {
            hookId = System.currentTimeMillis();
        }
        if (minorVersion == 0) {
            minorVersion = 14;
        }
        if (!StringUtils.hasText(url)) {
            url = "https://sandbox-quickbooks.api.intuit.com/v3/company";
        }
    }
}
