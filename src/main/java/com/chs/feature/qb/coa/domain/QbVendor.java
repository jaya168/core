package com.chs.feature.qb.coa.domain;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

@Data
@Embeddable
public class QbVendor {

    @NotBlank
    private String vendorId;

    @NotBlank
    private String vendorDisplayName;
}
