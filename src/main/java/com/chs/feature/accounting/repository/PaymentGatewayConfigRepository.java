package com.chs.feature.accounting.repository;

import com.chs.feature.accounting.domain.PaymentGatewayConfig;
import com.chs.feature.qb.configuration.domain.QbAccount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PaymentGatewayConfigRepository extends JpaRepository<PaymentGatewayConfig, Long> {
    List<PaymentGatewayConfig> findAllByAccount(QbAccount account);
}
