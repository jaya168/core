package com.chs.feature.accounting.domain;

import lombok.Getter;

@Getter
public enum JournalEntryPostingType {
    DEBIT("Debit"),
    CREDIT("Credit");

    private final String name;

    JournalEntryPostingType(String name) {
        this.name = name;
    }
}
