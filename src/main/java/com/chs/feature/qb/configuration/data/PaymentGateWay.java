package com.chs.feature.qb.configuration.data;

public enum PaymentGateWay {
    BONGLOY,
    WING,
    ABA,
    PIPAY
}
