package com.chs.organization.info.controller;

import com.chs.organization.info.domain.OrganizationDetail;
import com.chs.organization.info.repository.OrganizationDetailRepository;
import com.chs.persistence.exception.ResourceNotFoundException;
import com.chs.persistence.filter.FilterConfig;
import com.chs.persistence.service.EntityDataMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("organization")
@RequiredArgsConstructor
public class OrganizationDetailController {

    private final OrganizationDetailRepository repository;
    private final EntityDataMapper entityDataMapper;
    private final FilterConfig filterConfig;

    @PostMapping
    public OrganizationDetail create(@RequestBody @Valid OrganizationDetail organizationDetail) {
        return repository.save(organizationDetail);
    }

    @GetMapping
    public List<OrganizationDetail> get() {
        filterConfig.enableFilter();
        return repository.findAll();
    }

    @PutMapping("{id}")
    public OrganizationDetail update(@PathVariable Long id, @RequestBody OrganizationDetail organizationDetail) {
        filterConfig.enableFilter();
        var detail = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(OrganizationDetail.class, id));
        var newDetail = entityDataMapper.mapObject(organizationDetail, detail, OrganizationDetail.class);
        return repository.save(newDetail);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
        filterConfig.enableFilter();
        repository.deleteById(id);
    }
}
